﻿using FoodTruckPrism.DAL;
using FoodTruckPrism.Models;
using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace FoodTruckPrism.ViewModels
{
    public class CatalogViewModel : BindableBase
    {
        #region Properties

        private ObservableCollection<Item> _items;
        public ObservableCollection<Item> Items
        {
            get { return _items; }
            set { SetProperty(ref _items, value); }
        }
        /*
        private List<FoodType> _types;
        public List<FoodType> Types
        {
            get { return _types; }
            set { SetProperty(ref _types, value); }
        }

        private List<FoodFamily> _family;
        public List<FoodFamily> Family
        {
            get { return _family; }
            set { SetProperty(ref _family, value); }
        }
        */
        #endregion

        public CatalogViewModel()
        {
            ObservableCollection<Item> Items = ItemDAL.GetAll();

        }
    }
}
