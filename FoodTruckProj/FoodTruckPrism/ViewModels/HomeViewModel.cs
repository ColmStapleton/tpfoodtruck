﻿using FoodTruckPrism.DAL;
using FoodTruckPrism.Models;
using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FoodTruckPrism.ViewModels
{
    public class HomeViewModel : BindableBase
    {
        private News _currentNewsItem;
        public News CurrentNewsItem
        {
            get { return _currentNewsItem; }
            set { SetProperty(ref _currentNewsItem, value); }
        }

        private List<Item> _itemList;
        public List<Item> ItemList
        {
            get { return _itemList; }
            set { SetProperty(ref _itemList, value); }
        }

        public HomeViewModel()
        {
            CurrentNewsItem = NewsDAL.getRandomNews();
            ItemList = ItemDAL.getPopularItems("3");
        }
    }
}
