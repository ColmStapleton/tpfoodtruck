﻿using FoodTruckPrism.DAL;
using FoodTruckPrism.Models;
using FoodTruckPrism.Views;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Regions;
using System;
using System.Collections.Generic;

namespace FoodTruckPrism.ViewModels
{
    public class MainWindowViewModel : BindableBase
    {
        #region Properties
        private readonly IRegionManager _regionManager;

        private User _currentUser;
        public User CurrentUser
        {
            get { return _currentUser; }
            set { SetProperty(ref _currentUser, value); }
        }

        private string _loginPass;
        public string LoginPass
        {
            get { return _loginPass; }
            set { SetProperty(ref _loginPass, value); }
        }

        private string _loginEmail;
        public string LoginEmail
        {
            get { return _loginEmail; }
            set { SetProperty(ref _loginEmail, value); }
        }

        private Boolean _loggedIn;
        public Boolean LoggedIn
        {
            get { return _loggedIn; }
            set { SetProperty(ref _loggedIn, value); }
        }

        #endregion

        #region Commands

        private DelegateCommand _findUser;
        public DelegateCommand FindUser
        {
            get { return _findUser; }
        }

        private DelegateCommand _disconnectUser;
        public DelegateCommand DisconnectUser
        {
            get { return _disconnectUser; }
            set { SetProperty(ref _disconnectUser, value); }
        }

        private DelegateCommand _openRegister;
        public DelegateCommand OpenRegister
        {
            get { return _openRegister; }
            set { SetProperty(ref _openRegister, value); }
        }

        private DelegateCommand _openCatalog;
        public DelegateCommand OpenCatalog
        {
            get { return _openCatalog; }
            set { SetProperty(ref _openCatalog, value); }
        }

        public DelegateCommand<string> NavigateCommand { get; private set; }
        #endregion

        public MainWindowViewModel(IRegionManager regionManager)
        {
            User CurrentUser = new User();
            LoggedIn = false;
            _findUser = new DelegateCommand(DoFindUser);
            _disconnectUser = new DelegateCommand(DoDisconnectUser);
            _regionManager = regionManager;
            NavigateCommand = new DelegateCommand<string>(DoNavigate);
            _regionManager.RegisterViewWithRegion("ContentRegion", typeof(Home));
        }


        #region Methods

        protected void DoDisconnectUser()
        {
            CurrentUser = new User();
            LoggedIn = false;
        }

        protected void DoNavigate(string navigatePath)
        {
            if (navigatePath != null)
            {
                _regionManager.RequestNavigate("ContentRegion", navigatePath);
            }
        }

        protected void DoFindUser()
        {
            if (LoginEmail.Length > 0 && LoginPass.Length > 0)
            {
                CurrentUser = UserDAL.FindByEmail(LoginEmail, LoginPass);

                LoginEmail = "";
                LoginPass = "";
            }

            if (CurrentUser is null)
            {
                LoggedIn = false;
            }
            else
            {
                LoggedIn = true;
            }
        }
        #endregion

    }
}
