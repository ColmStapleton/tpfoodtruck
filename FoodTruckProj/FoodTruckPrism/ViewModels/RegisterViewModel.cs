﻿using FoodTruckPrism.DAL;
using FoodTruckPrism.Models;
using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FoodTruckPrism.ViewModels
{
    public class RegisterViewModel : BindableBase
    {
        #region Properties

        private User _newUser;
        public User NewUser
        {
            get { return _newUser; }
            set { SetProperty(ref _newUser, value); }
        }

        private Boolean _userGender;
        public Boolean UserGender
        {
            get { return _userGender; }
            set { SetProperty(ref _userGender, value); }
        }

        #endregion

        #region Commands
        private DelegateCommand _saveUser;
        public DelegateCommand SaveUser =>
            _saveUser ?? (_saveUser = new DelegateCommand(ExecuteSaveUser, CanExecuteSaveUser));

        void ExecuteSaveUser()
        {
            if (UserGender == true)
            {
                NewUser.Gender = 2;
            }
            else if (UserGender == false)
            {
                NewUser.Gender = 1;
            }
            UserDAL.InsertToDB(NewUser);
            new RegisterViewModel();
        }

        bool CanExecuteSaveUser()
        {
            return true;
        }
        #endregion

        public RegisterViewModel()
        {
            NewUser = new User();
            NewUser.Dob = new DateTime(2000, 1, 1);
            NewUser.Profile = 1;
            UserGender = true;

        }
    }
}
