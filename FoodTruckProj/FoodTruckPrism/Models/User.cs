﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodTruckPrism.Models
{
    public class User : BindableBase
    {

        private int _id;
        public int Id
        {
            get { return _id; }
            set { SetProperty(ref _id, value); }
        }

        private int _gender;
        public int Gender
        {
            get { return _gender; }
            set { SetProperty(ref _gender, value); }
        }

        private int _profile;
        public int Profile
        {
            get { return _profile; }
            set { SetProperty(ref _profile, value); }
        }

        private int _company;
        public int Company
        {
            get { return _company; }
            set { SetProperty(ref _company, value); }
        }

        private string _firstName;
        public string FirstName
        {
            get { return _firstName; }
            set { SetProperty(ref _firstName, value); }
        }

        private string _lastName;
        public string LastName
        {
            get { return _lastName; }
            set { SetProperty(ref _lastName, value); }
        }

        private string _password;
        public string Password
        {
            get { return _password; }
            set { SetProperty(ref _password, value); }
        }

        private string _email;
        public string Email
        {
            get { return _email; }
            set { SetProperty(ref _email, value); }
        }

        private DateTime _dob;
        public DateTime Dob
        {
            get { return _dob; }
            set { SetProperty(ref _dob, value); }
        }

        private string _phone;
        public string Phone
        {
            get { return _phone; }
            set { SetProperty(ref _phone, value); }
        }

        public User()
        {

        }

        public User(int gender, int profile, int company, string firstN, string lastN, string pass, string email, DateTime dob, string phone)
        {
            this.Gender = gender;
            this.FirstName = firstN;
            this.LastName = lastN;
            this.Profile = profile;
            this.Company = company;
            this.Password = pass;
            this.Email = email;
            this.Dob = dob;
            this.Phone = phone;
        }
    }
}
