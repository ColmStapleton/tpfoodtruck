﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodTruckPrism.Models
{
    public class Item : BindableBase
    {

        private int _id;
        public int Id
        {
            get { return _id; }
            set { SetProperty(ref _id, value); }
        }

        private int _type;
        public int Type
        {
            get { return _type; }
            set { SetProperty(ref _type, value); }
        }

        private string _itemName;
        public string ItemName
        {
            get { return _itemName; }
            set { SetProperty(ref _itemName, value); }
        }

        private string _description;
        public string Description
        {
            get { return _description; }
            set { SetProperty(ref _description, value); }
        }

        private int _nbSales;
        public int NbSales
        {
            get { return _nbSales; }
            set { SetProperty(ref _nbSales, value); }
        }

        private decimal _price;
        public decimal Price
        {
            get { return _price; }
            set { SetProperty(ref _price, value); }
        }

        private string _urlImage;
        public string UrlImage
        {
            get { return _urlImage; }
            set { SetProperty(ref _urlImage, value); }
        }

        private int _stock;
        public int Stock
        {
            get { return _stock; }
            set { SetProperty(ref _stock, value); }
        }

        private string _lmmjvsd;
        public string Lmmjvsd
        {
            get { return _lmmjvsd; }
            set { SetProperty(ref _lmmjvsd, value); }
        }

        private double _rating;
        public double Rating
        {
            get { return _rating; }
            set { SetProperty(ref _rating, value); }
        }

        private string _unit;
        public string Unit
        {
            get { return _unit; }
            set { SetProperty(ref _unit, value); }
        }
    }
}
