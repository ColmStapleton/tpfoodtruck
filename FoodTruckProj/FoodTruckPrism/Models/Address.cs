﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodTruckPrism.Models
{
    public class Address
    {
        private int _id;

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private User _owner;

        public User Owner
        {
            get { return _owner; }
            set { _owner = value; }
        }

        private string _tag;

        public string Tag
        {
            get { return _tag; }
            set { _tag = value; }
        }

        private Boolean _active;

        public Boolean Active
        {
            get { return _active; }
            set { _active = value; }
        }

        private int _num;

        public int Num
        {
            get { return _num; }
            set { _num = value; }
        }

        private string _street;

        public string Street
        {
            get { return _street; }
            set { _street = value; }
        }

        private string _postCode;

        public string PostCode
        {
            get { return _postCode; }
            set { _postCode = value; }
        }

        private string _town;

        public string Town
        {
            get { return _town; }
            set { _town = value; }
        }

        private string _country;

        public string Country
        {
            get { return _country; }
            set { _country = value; }
        }

    }
}
