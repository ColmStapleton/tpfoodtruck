﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodTruckPrism.Models
{
    public class News : BindableBase
    {

        private int _id;
        public int Id
        {
            get { return _id; }
            set { SetProperty(ref _id, value); }
        }

        private string _title;
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        private string _description;
        public string Description
        {
            get { return _description; }
            set { SetProperty(ref _description, value); }
        }

        private string _url;
        public string Url
        {
            get { return _url; }
            set { SetProperty(ref _url, value); }
        }

        private DateTime _dateStart;
        public DateTime DateStart
        {
            get { return _dateStart; }
            set { SetProperty(ref _dateStart, value); }
        }

        private DateTime _dateEnd;
        public DateTime DateEnd
        {
            get { return _dateEnd; }
            set { SetProperty(ref _dateEnd, value); }
        }

        private string _urlImage;
        public string UrlImage
        {
            get { return _urlImage; }
            set { SetProperty(ref _urlImage, value); }
        }

        private User _owner;
        public User Owner
        {
            get { return _owner; }
            set { SetProperty(ref _owner, value); }
        }

        private Boolean _active;
        public Boolean Active
        {
            get { return _active; }
            set { SetProperty(ref _active, value); }
        }
    }
}
