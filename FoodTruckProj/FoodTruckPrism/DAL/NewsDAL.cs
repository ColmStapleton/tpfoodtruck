﻿using FoodTruckPrism.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodTruckPrism.DAL
{
    public class NewsDAL
    {

        private static Random r = new Random();


        public static News getRandomNews()
        {
            List<News> lNews = new List<News>();

            using (SqlConnection connect = ConnectToDB())
            {
                using (SqlCommand command = connect.CreateCommand())
                {
                    command.CommandText = String.Format("SELECT * FROM ACTUALITES");

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            News news = new News()
                            {
                                Id = (int)reader["ID_ACTUALITE"],
                                Title = reader["TITRE"].ToString(),
                                Description = reader["DESCRIPTION"].ToString(),
                                Url = reader["LINK"].ToString(),
                                UrlImage = reader["URL_IMAGE"].ToString(),
                                DateStart = (DateTime)reader["DATE_DEBUT"],
                                DateEnd = (DateTime)reader["DATE_FIN"],
                                Owner = UserDAL.FindById((int)reader["ID_UTILISATEUR"]),
                                Active = (Boolean)reader["ACTIF"]
                            };

                            lNews.Add(news);
                        }
                    }
                }
            }
            //Get random item from list of news
            if (lNews.Count > 0)
            {
                return lNews[r.Next(lNews.Count)];
            }
            else
            {
                return null;
            }

        }

        private static SqlConnection ConnectToDB()
        {
            SqlConnection connect = new SqlConnection();
            ConnectionStringSettings connex = ConfigurationManager.ConnectionStrings["ConnectionFoodTruck"];

            connect.ConnectionString = connex.ConnectionString;
            connect.Open();
            return connect;
        }
    }
}
