﻿using FoodTruckPrism.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodTruckPrism.DAL
{
    public class UserDAL
    {

        public static void InsertToDB(User u)
        {
            using (SqlConnection connect = ConnectToDB())
            {
                using (SqlCommand command = connect.CreateCommand())
                {
                    command.CommandText = "INSERT INTO UTILISATEUR (ID_GENRE, ID_PROFIL, NOM, PRENOM, MOT_DE_PASSE, EMAIL, DATE_DE_NAISSANCE, TEL) " +
                "VALUES (@Gender,@Profil,@LastName,@FirstName,@Password,@Email,@Date,@Phone)";
                    command.Parameters.AddWithValue("@Gender", u.Gender);
                    command.Parameters.AddWithValue("@Profil", u.Profile);
                    command.Parameters.AddWithValue("@LastName", u.LastName);
                    command.Parameters.AddWithValue("@FirstName", u.FirstName);
                    command.Parameters.AddWithValue("@Password", u.Password);
                    command.Parameters.AddWithValue("@Date", u.Dob);
                    command.Parameters.AddWithValue("@Email", u.Email);
                    command.Parameters.AddWithValue("@Phone", u.Phone);

                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.InsertCommand = command;
                    dataAdapter.InsertCommand.ExecuteNonQuery();
                }
            }
        }

        public static User FindById(int _id)
        {
            User owner = new User();

            using (SqlConnection connect = ConnectToDB())
            {
                using (SqlCommand command = connect.CreateCommand())
                {
                    command.CommandText = "SELECT * FROM UTILISATEUR Where ID_UTILISATEUR = @UserId";
                    command.Parameters.AddWithValue("@UserId", _id);

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            owner = new User()
                            {
                                Id = (int)reader["ID_UTILISATEUR"],
                                Gender = (int)reader["ID_GENRE"],
                                //Company = CompanyDAL.FindById((int)reader["ID_SOCIETE"]),
                                Profile = (int)reader["ID_PROFIL"],
                                LastName = reader["NOM"].ToString(),
                                FirstName = reader["PRENOM"].ToString(),
                                Password = reader["MOT_DE_PASSE"].ToString(),
                                Email = reader["EMAIL"].ToString(),
                                Dob = (DateTime)reader["DATE_DE_NAISSANCE"],
                                Phone = reader["TEL"].ToString()
                            };
                        }
                        else
                        {
                            Console.WriteLine("Il n'y a pas d'utilisateur ayant l'id {0} dans la table.", _id);
                        }
                    }
                }
            }

            return owner;
        }

        public static User FindByEmail(string loginEmail, string loginPass)
        {
            User currentUser = new User();
            using (SqlConnection connect = ConnectToDB())
            {
                using (SqlCommand command = connect.CreateCommand())
                {
                    command.CommandText = "SELECT * FROM UTILISATEUR Where EMAIL = @EMAIL AND MOT_DE_PASSE = @PASSWORD";
                    command.Parameters.AddWithValue("@EMAIL", loginEmail);
                    command.Parameters.AddWithValue("@PASSWORD", loginPass);

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            currentUser = new User()
                            {
                                Id = (int)reader["ID_UTILISATEUR"],
                                Gender = (int)reader["ID_GENRE"],
                                //Company = CompanyDAL.FindById((int)reader["ID_SOCIETE"]),
                                Profile = (int)reader["ID_PROFIL"],
                                LastName = reader["NOM"].ToString(),
                                FirstName = reader["PRENOM"].ToString(),
                                Password = reader["MOT_DE_PASSE"].ToString(),
                                Email = reader["EMAIL"].ToString(),
                                Dob = (DateTime)reader["DATE_DE_NAISSANCE"],
                                Phone = reader["TEL"].ToString()
                            };
                        }
                    }
                }
            }

            return currentUser;
        }

        private static SqlConnection ConnectToDB()
        {
            SqlConnection connect = new SqlConnection();
            ConnectionStringSettings connex = ConfigurationManager.ConnectionStrings["ConnectionFoodTruck"];

            connect.ConnectionString = connex.ConnectionString;
            connect.Open();
            return connect;
        }
    }
}
