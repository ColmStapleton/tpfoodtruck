﻿using FoodTruckPrism.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodTruckPrism.DAL
{
    public class AddressDAL
    {
        public static List<Address> FindAll()
        {
            List<Address> addresses = new List<Address>();
            using (SqlConnection connect = ConnectToDB())
            {
                using (SqlCommand command = connect.CreateCommand())
                {
                    command.CommandText=("SELECT * FROM ADRESSE");
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Address address = new Address()
                            {
                                Id = (int)reader["ID_ADRESSE"],
                                Owner = UserDAL.FindById((int)reader["ID_UTILISATEUR"]),
                                Tag = reader["LIBELLE_ADRESSE"].ToString(),
                                Active = (Boolean)reader["ACTIF"],
                                Num = (int)reader["NUMERO_RUE"],
                                Street = reader["RUE"].ToString(),
                                PostCode = reader["CODE_POSTAL"].ToString(),
                                Town = reader["VILLE"].ToString(),
                                Country = reader["PAYS"].ToString()
                            };
                            addresses.Add(address);
                        }
                    }
                }
            }

            return addresses;
        }

        public static Address FindById(int _id)
        {
            Address address = new Address();

            using (SqlConnection connect = ConnectToDB())
            {
                using (SqlCommand command = connect.CreateCommand())
                {
                    command.CommandText = ("SELECT * FROM ADRESSE Where ID_ADRESSE = @AdresseId");
                    command.Parameters.AddWithValue("@AddressId", _id);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            address = new Address()
                            {
                                Id = (int)reader["ID_ADRESSE"],
                                Owner = UserDAL.FindById((int)reader["ID_UTILISATEUR"]),
                                Tag = reader["LIBELLE_ADRESSE"].ToString(),
                                Active = (Boolean)reader["ACTIF"],
                                Num = (int)reader["NUMERO_RUE"],
                                Street = reader["RUE"].ToString(),
                                PostCode = reader["CODE_POSTAL"].ToString(),
                                Town = reader["VILLE"].ToString(),
                                Country = reader["PAYS"].ToString()
                            };
                        }
                        else
                        {
                            Console.WriteLine("Il n'y a pas d'adresse ayant l'id {0} dans la table.", _id);
                        }
                    }
                }
            } 
            return address;
        }

        public void InsertToDb(Address a)
        {
            using (SqlConnection connect = ConnectToDB())
            {
                using (SqlCommand command = connect.CreateCommand())
                {
                    command.CommandText=("INSERT INTO ADRESSE (ID_UTILISATEUR, LIBELLE_ADRESSE, ACTIF, NUMERO_RUE, RUE, CODE_POSTAL, VILLE, PAYS) " +
                "VALUES (@UserId,@Name,@Activ,@Num,@Street,@PostCode,@Town,@Country)");
                    command.Parameters.AddWithValue("@UserId", a.Owner.Id);
                    command.Parameters.AddWithValue("@Name", a.Tag);
                    command.Parameters.AddWithValue("@Activ", a.Active);
                    command.Parameters.AddWithValue("@Num", a.Num);
                    command.Parameters.AddWithValue("@Street", a.Street);
                    command.Parameters.AddWithValue("@PostCode", a.PostCode);
                    command.Parameters.AddWithValue("@Town", a.Town);
                   command.Parameters.AddWithValue("@Country", a.Country);

                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.InsertCommand = command;
                    dataAdapter.InsertCommand.ExecuteNonQuery();
                }
            }
        }


        private static SqlConnection ConnectToDB()
        {
            SqlConnection connect = new SqlConnection();
            ConnectionStringSettings connex = ConfigurationManager.ConnectionStrings["ConnectionFoodTruck"];

            connect.ConnectionString = connex.ConnectionString;
            connect.Open();
            return connect;
        }

    }
}
