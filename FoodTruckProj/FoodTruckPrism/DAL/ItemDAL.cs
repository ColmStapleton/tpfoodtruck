﻿using FoodTruckPrism.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodTruckPrism.DAL
{
    public class ItemDAL
    {
        public static List<Item> getPopularItems(string _num)
        {
            List<Item> lItem = new List<Item>();

            using (SqlConnection connect = ConnectToDB())
            {
                using (SqlCommand command = connect.CreateCommand())
                {
                    command.CommandText = String.Format("SELECT TOP {0} * FROM PRODUIT ORDER BY NOMBRE_DE_VENTE DESC", _num);

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Item item = new Item()
                            {
                                Id = (int)reader["ID_PRODUIT"],
                                Type = (int)reader["ID_FAMILLE_REPAS"],
                                ItemName = reader["LIBELLE_PRODUIT"].ToString(),
                                Description = reader["DESCRIPTION"].ToString(),
                                NbSales = (int)reader["NOMBRE_DE_VENTE"],
                                Price = (decimal)reader["PRIX"],
                                UrlImage = reader["URL_IMAGE"].ToString(),
                                Stock = (int)reader["STOCK"],
                                Lmmjvsd = reader["LMMJVSD"].ToString(),
                                //Rating = (double)reader["MOYENNE_NOTE"],
                                //Unit = reader["UNITE"].ToString()
                            };

                            lItem.Add(item);
                        }
                    }
                }
            }

            return lItem;
        }

        public static ObservableCollection<Item> GetAll()
        {
            ObservableCollection<Item> lItem = new ObservableCollection<Item>();

            using (SqlConnection connect = ConnectToDB())
            {
                using (SqlCommand command = connect.CreateCommand())
                {
                    command.CommandText = String.Format("SELECT * FROM PRODUIT");

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Item item = new Item()
                            {
                                Id = (int)reader["ID_PRODUIT"],
                                Type = (int)reader["ID_FAMILLE_REPAS"],
                                ItemName = reader["LIBELLE_PRODUIT"].ToString(),
                                Description = reader["DESCRIPTION"].ToString(),
                                NbSales = (int)reader["NOMBRE_DE_VENTE"],
                                Price = (decimal)reader["PRIX"],
                                UrlImage = reader["URL_IMAGE"].ToString(),
                                Stock = (int)reader["STOCK"],
                                Lmmjvsd = reader["LMMJVSD"].ToString(),
                                //Rating = (double)reader["MOYENNE_NOTE"],
                                //Unit = reader["UNITE"].ToString()
                            };

                            lItem.Add(item);
                        }
                    }
                }
            }

            return lItem;
        }

        public static ObservableCollection<Item> GetAllFilter(int typeId)
        {
            ObservableCollection<Item> lItem = new ObservableCollection<Item>();

            using (SqlConnection connect = ConnectToDB())
            {
                using (SqlCommand command = connect.CreateCommand())
                {
                    command.CommandText = String.Format("SELECT * FROM PRODUIT Where ID_FAMILLE_REPAS = @Type");
                    command.Parameters.AddWithValue("@Type", typeId);

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Item item = new Item()
                            {
                                Id = (int)reader["ID_PRODUIT"],
                                Type = (int)reader["ID_FAMILLE_REPAS"],
                                ItemName = reader["LIBELLE_PRODUIT"].ToString(),
                                Description = reader["DESCRIPTION"].ToString(),
                                NbSales = (int)reader["NOMBRE_DE_VENTE"],
                                Price = (decimal)reader["PRIX"],
                                UrlImage = reader["URL_IMAGE"].ToString(),
                                Stock = (int)reader["STOCK"],
                                Lmmjvsd = reader["LMMJVSD"].ToString(),
                                //Rating = (double)reader["MOYENNE_NOTE"],
                                //Unit = reader["UNITE"].ToString()
                            };

                            lItem.Add(item);
                        }
                    }
                }
            }

            return lItem;
        }
        private static SqlConnection ConnectToDB()
        {
            SqlConnection connect = new SqlConnection();
            ConnectionStringSettings connex = ConfigurationManager.ConnectionStrings["ConnectionFoodTruck"];

            connect.ConnectionString = connex.ConnectionString;
            connect.Open();
            return connect;
        }
    }
}
