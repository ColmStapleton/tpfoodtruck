﻿using FoodTruckPrism.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodTruckPrism.DAL
{
    public class CompanyDAL
    {
        //Returns ID
        public static int FindByName(string _name)
        {
            int Id =0;

            using (SqlConnection connect = ConnectToDB())
            {
                using (SqlCommand command = connect.CreateCommand())
                {
                    command.CommandText="SELECT ID_SOCIETE FROM SOCIETE Where LIBELLE_SOCIETE = @CompanyName";
                    command.Parameters.AddWithValue("@CompanyName", _name);

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            Id = (int)reader["ID_SOCIETE"];
                        }
                        else
                        {
                            Console.WriteLine("Il n'y a pas de société ayant le nom {0} dans la table.", _name);
                        }
                    }
                }
            }
            return Id;
        }

        //Returns Company
        public static Company FindById(int _id)
        {
            Company company = new Company();

            using (SqlConnection connect = ConnectToDB())
            {
                using (SqlCommand command = connect.CreateCommand())
                {
                    command.CommandText="SELECT * FROM SOCIETE Where ID_SOCIETE = @Company_ID";
                    command.Parameters.AddWithValue("@Company_ID", _id);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            company = new Company()
                            {
                                Id = (int)reader["ID_SOCIETE"],
                                Name = reader["LIBELLE_SOCIETE"].ToString(),
                                Num = (int)reader["NUMERO_RUE"],
                                Street = reader["RUE"].ToString(),
                                PostCode = reader["CODE_POSTAL"].ToString(),
                                Town = reader["VILLE"].ToString(),
                                Country = reader["PAYS"].ToString()
                            };
                        }
                        else
                        {
                            Console.WriteLine("Il n'y a pas de société ayant l'id {0} dans la table.", _id);
                        }
                    }
                }
            }
            return company;
        }

        public static void InsertToDb(Company s)
        {
            using (SqlConnection connect = ConnectToDB())
            {
                using (SqlCommand command = connect.CreateCommand())
                {
                    command.CommandText="INSERT INTO SOCIETE (LIBELLE_SOCIETE, NUMERO_RUE, RUE, CODE_POSTAL, VILLE, PAYS) " +
                "VALUES (@Name,@Num,@Street,@PostCode,@Town,@Country)";
                    command.Parameters.AddWithValue("@Name", s.Name);
                    command.Parameters.AddWithValue("@Num", s.Num);
                    command.Parameters.AddWithValue("@Street", s.Street);
                    command.Parameters.AddWithValue("@PostCode", s.PostCode);
                    command.Parameters.AddWithValue("@Town", s.Town);
                    command.Parameters.AddWithValue("@Country", s.Country);

                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.InsertCommand = command;
                    dataAdapter.InsertCommand.ExecuteNonQuery();
                }
            }
        }


        private static SqlConnection ConnectToDB()
        {
            SqlConnection connect = new SqlConnection();
            ConnectionStringSettings connex = ConfigurationManager.ConnectionStrings["ConnectionFoodTruck"];

            connect.ConnectionString = connex.ConnectionString;
            connect.Open();
            return connect;
        }
    }
}
