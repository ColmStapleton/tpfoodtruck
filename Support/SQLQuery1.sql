USE [FOODTRUCK]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PRODUIT]') AND type in (N'U'))
ALTER TABLE [dbo].[PRODUIT] DROP CONSTRAINT IF EXISTS [DF_PRODUIT_NOMBRE_DE_VENTE]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[NOTE]') AND type in (N'U'))
ALTER TABLE [dbo].[NOTE] DROP CONSTRAINT IF EXISTS [DF_NOTE_ACTIF]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FACTURE]') AND type in (N'U'))
ALTER TABLE [dbo].[FACTURE] DROP CONSTRAINT IF EXISTS [DF_Table_1_FACTURE]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ACTUALITES]') AND type in (N'U'))
ALTER TABLE [dbo].[ACTUALITES] DROP CONSTRAINT IF EXISTS [DF_ACTUALITES_ACTIF]
GO
/****** Object:  Table [dbo].[UTILISATEUR]    Script Date: 17/09/2019 15:51:05 ******/
DROP TABLE IF EXISTS [dbo].[UTILISATEUR]
GO
/****** Object:  Table [dbo].[TYPE_REPAS]    Script Date: 17/09/2019 15:51:05 ******/
DROP TABLE IF EXISTS [dbo].[TYPE_REPAS]
GO
/****** Object:  Table [dbo].[TYPE_FAMILLE_REPAS]    Script Date: 17/09/2019 15:51:05 ******/
DROP TABLE IF EXISTS [dbo].[TYPE_FAMILLE_REPAS]
GO
/****** Object:  Table [dbo].[SOCIETE]    Script Date: 17/09/2019 15:51:05 ******/
DROP TABLE IF EXISTS [dbo].[SOCIETE]
GO
/****** Object:  Table [dbo].[PROFIL]    Script Date: 17/09/2019 15:51:05 ******/
DROP TABLE IF EXISTS [dbo].[PROFIL]
GO
/****** Object:  Table [dbo].[PRODUIT_ALLERGENE]    Script Date: 17/09/2019 15:51:05 ******/
DROP TABLE IF EXISTS [dbo].[PRODUIT_ALLERGENE]
GO
/****** Object:  Table [dbo].[PRODUIT]    Script Date: 17/09/2019 15:51:05 ******/
DROP TABLE IF EXISTS [dbo].[PRODUIT]
GO
/****** Object:  Table [dbo].[PARAMETRAGE]    Script Date: 17/09/2019 15:51:05 ******/
DROP TABLE IF EXISTS [dbo].[PARAMETRAGE]
GO
/****** Object:  Table [dbo].[NOTE]    Script Date: 17/09/2019 15:51:05 ******/
DROP TABLE IF EXISTS [dbo].[NOTE]
GO
/****** Object:  Table [dbo].[LIGNE_FACTURE]    Script Date: 17/09/2019 15:51:05 ******/
DROP TABLE IF EXISTS [dbo].[LIGNE_FACTURE]
GO
/****** Object:  Table [dbo].[LIGNE_COMMANDE]    Script Date: 17/09/2019 15:51:05 ******/
DROP TABLE IF EXISTS [dbo].[LIGNE_COMMANDE]
GO
/****** Object:  Table [dbo].[GENRE]    Script Date: 17/09/2019 15:51:05 ******/
DROP TABLE IF EXISTS [dbo].[GENRE]
GO
/****** Object:  Table [dbo].[FAMILLE_REPAS]    Script Date: 17/09/2019 15:51:05 ******/
DROP TABLE IF EXISTS [dbo].[FAMILLE_REPAS]
GO
/****** Object:  Table [dbo].[FACTURE]    Script Date: 17/09/2019 15:51:05 ******/
DROP TABLE IF EXISTS [dbo].[FACTURE]
GO
/****** Object:  Table [dbo].[COMMANDE_STATUT]    Script Date: 17/09/2019 15:51:05 ******/
DROP TABLE IF EXISTS [dbo].[COMMANDE_STATUT]
GO
/****** Object:  Table [dbo].[COMMANDE]    Script Date: 17/09/2019 15:51:05 ******/
DROP TABLE IF EXISTS [dbo].[COMMANDE]
GO
/****** Object:  Table [dbo].[ALLERGENE]    Script Date: 17/09/2019 15:51:05 ******/
DROP TABLE IF EXISTS [dbo].[ALLERGENE]
GO
/****** Object:  Table [dbo].[ADRESSE]    Script Date: 17/09/2019 15:51:05 ******/
DROP TABLE IF EXISTS [dbo].[ADRESSE]
GO
/****** Object:  Table [dbo].[ACTUALITES]    Script Date: 17/09/2019 15:51:05 ******/
DROP TABLE IF EXISTS [dbo].[ACTUALITES]
GO
/****** Object:  Table [dbo].[ACTUALITES]    Script Date: 17/09/2019 15:51:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ACTUALITES](
	[ID_ACTUALITE] [int] IDENTITY(1,1) NOT NULL,
	[TITRE] [varchar](255) NOT NULL,
	[DESCRIPTION] [text] NOT NULL,
	[URL_IMAGE] [varchar](255) NOT NULL,
	[DATE_DEBUT] [datetime] NOT NULL,
	[DATE_FIN] [datetime] NOT NULL,
	[LINK] [varchar](255) NOT NULL,
	[ID_UTILISATEUR] [int] NOT NULL,
	[ACTIF] [bit] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ADRESSE]    Script Date: 17/09/2019 15:51:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ADRESSE](
	[ID_ADRESSE] [int] NOT NULL,
	[ID_UTILISATEUR] [int] NOT NULL,
	[LIBELLE_ADRESSE] [varchar](50) NOT NULL,
	[ACTIF] [bit] NOT NULL,
	[NUMERO_RUE] [varchar](10) NOT NULL,
	[RUE] [varchar](30) NOT NULL,
	[CODE_POSTAL] [varchar](6) NOT NULL,
	[VILLE] [varchar](50) NOT NULL,
	[PAYS] [varchar](30) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ALLERGENE]    Script Date: 17/09/2019 15:51:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ALLERGENE](
	[ID_ALLERGENE] [int] IDENTITY(1,1) NOT NULL,
	[LIBELLE_ALLERGENE] [varchar](255) NOT NULL,
	[CODE_ALLERGENE] [varchar](5) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[COMMANDE]    Script Date: 17/09/2019 15:51:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[COMMANDE](
	[ID_COMMANDE] [int] IDENTITY(1,1) NOT NULL,
	[ID_UTILISATEUR] [int] NOT NULL,
	[ID_ADRESSE] [int] NOT NULL,
	[DATE_COMMANDE] [datetime] NOT NULL,
	[TOTAL_COMMANDE] [numeric](18, 2) NOT NULL,
	[ID_COMMANDE_STATUS] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[COMMANDE_STATUT]    Script Date: 17/09/2019 15:51:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[COMMANDE_STATUT](
	[ID_COMMANDE_STATUT] [int] IDENTITY(1,1) NOT NULL,
	[LIBELLE_COMMANDE_STATUT] [varchar](50) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FACTURE]    Script Date: 17/09/2019 15:51:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FACTURE](
	[ID_FACTURE] [int] IDENTITY(1,1) NOT NULL,
	[ID_COMMANDE] [int] NOT NULL,
	[ID_ADRESSE] [int] NOT NULL,
	[TOTAL_FACTURE] [numeric](18, 2) NOT NULL,
	[FACTUREE] [bit] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FAMILLE_REPAS]    Script Date: 17/09/2019 15:51:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FAMILLE_REPAS](
	[ID_FAMILLE_REPAS] [int] IDENTITY(1,1) NOT NULL,
	[LIBELLE_FAMILLE_REPAS] [varchar](255) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[GENRE]    Script Date: 17/09/2019 15:51:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GENRE](
	[ID_GENRE] [int] IDENTITY(1,1) NOT NULL,
	[LIBELLE_GENRE] [varchar](20) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LIGNE_COMMANDE]    Script Date: 17/09/2019 15:51:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LIGNE_COMMANDE](
	[ID_LIGNE_COMMANDE] [int] NOT NULL,
	[ID_COMMANDE] [int] NOT NULL,
	[ID_PRODUIT] [int] NOT NULL,
	[QTE] [float] NOT NULL,
	[PRIX_UNITAIRE] [numeric](18, 2) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LIGNE_FACTURE]    Script Date: 17/09/2019 15:51:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LIGNE_FACTURE](
	[ID_LIGNE_FACTURE] [int] IDENTITY(1,1) NOT NULL,
	[ID_FACTURE] [int] NOT NULL,
	[QUANTITE] [float] NOT NULL,
	[PRIX_UNITAIRE] [numeric](18, 2) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NOTE]    Script Date: 17/09/2019 15:51:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NOTE](
	[ID_NOTE] [int] IDENTITY(1,1) NOT NULL,
	[ID_PRODUIT] [int] NOT NULL,
	[ID_COMMANDE] [int] NOT NULL,
	[NOTE] [float] NOT NULL,
	[DATE_NOTE] [datetime] NOT NULL,
	[DESCRIPTION] [text] NULL,
	[ACTIF] [bit] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PARAMETRAGE]    Script Date: 17/09/2019 15:51:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PARAMETRAGE](
	[ID_PARAMETRAGE] [int] IDENTITY(1,1) NOT NULL,
	[PLAGE] [varchar](255) NOT NULL,
	[PRIX] [numeric](18, 2) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PRODUIT]    Script Date: 17/09/2019 15:51:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PRODUIT](
	[ID_PRODUIT] [int] IDENTITY(1,1) NOT NULL,
	[ID_FAMILLE_REPAS] [int] NOT NULL,
	[LIBELLE_PRODUIT] [varchar](30) NOT NULL,
	[DESCRIPTION] [text] NULL,
	[NOMBRE_DE_VENTE] [int] NOT NULL,
	[PRIX] [numeric](18, 2) NOT NULL,
	[URL_IMAGE] [varchar](255) NOT NULL,
	[STOCK] [int] NOT NULL,
	[LMMJVSD] [varchar](7) NULL,
	[MOYENNE_NOTE] [float] NULL,
	[UNITE] [varchar](10) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PRODUIT_ALLERGENE]    Script Date: 17/09/2019 15:51:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PRODUIT_ALLERGENE](
	[ID_PRODUIT] [int] NOT NULL,
	[ID_ALLERGENE] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PROFIL]    Script Date: 17/09/2019 15:51:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PROFIL](
	[ID_PROFIL] [int] IDENTITY(1,1) NOT NULL,
	[LIBELLE_PROFIL] [varchar](30) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SOCIETE]    Script Date: 17/09/2019 15:51:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SOCIETE](
	[ID_SOCIETE] [int] IDENTITY(1,1) NOT NULL,
	[LIBELLE_SOCIETE] [varchar](50) NOT NULL,
	[NUMERO_RUE] [varchar](10) NOT NULL,
	[RUE] [varchar](30) NOT NULL,
	[CODE_POSTAL] [varchar](6) NOT NULL,
	[VILLE] [varchar](50) NOT NULL,
	[PAYS] [varchar](30) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TYPE_FAMILLE_REPAS]    Script Date: 17/09/2019 15:51:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TYPE_FAMILLE_REPAS](
	[ID_TYPE_REPAS] [int] NOT NULL,
	[ID_FAMILLE_REPAS] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TYPE_REPAS]    Script Date: 17/09/2019 15:51:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TYPE_REPAS](
	[ID_TYPE_REPAS] [int] IDENTITY(1,1) NOT NULL,
	[LIBELLE_REPAS] [varchar](255) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UTILISATEUR]    Script Date: 17/09/2019 15:51:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UTILISATEUR](
	[ID_UTILISATEUR] [int] IDENTITY(1,1) NOT NULL,
	[ID_GENRE] [int] NOT NULL,
	[ID_PROFIL] [int] NOT NULL,
	[ID_SOCIETE] [int] NULL,
	[NOM] [varchar](30) NOT NULL,
	[PRENOM] [varchar](50) NOT NULL,
	[MOT_DE_PASSE] [varchar](255) NOT NULL,
	[EMAIL] [varchar](100) NOT NULL,
	[DATE_DE_NAISSANCE] [date] NOT NULL,
	[TEL] [varchar](20) NULL
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[ACTUALITES] ON 
GO
INSERT [dbo].[ACTUALITES] ([ID_ACTUALITE], [TITRE], [DESCRIPTION], [URL_IMAGE], [DATE_DEBUT], [DATE_FIN], [LINK], [ID_UTILISATEUR], [ACTIF]) VALUES (1, N'Manger vegan à Barcelone', N'Barcelone est une ville très connue pour sa gastronomie, notamment pour ses tapas. Et ces derniers existent en de nombreuses versions depuis plusieurs années.', N'https://www.vegactu.com/wp-content/uploads/2019/05/flaxkale.jpg', CAST(N'2019-01-01T00:00:00.000' AS DateTime), CAST(N'2029-12-31T00:00:00.000' AS DateTime), N'https://www.vegactu.com/actualite/style-de-vie/manger-vegan-a-barcelone-28405/', 1, 1)
GO
INSERT [dbo].[ACTUALITES] ([ID_ACTUALITE], [TITRE], [DESCRIPTION], [URL_IMAGE], [DATE_DEBUT], [DATE_FIN], [LINK], [ID_UTILISATEUR], [ACTIF]) VALUES (2, N'Attention au sucre', N'Il faut faire attention ....', N'https://www.vegactu.com/wp-content/uploads/2019/05/flaxkale.jpg', CAST(N'2019-01-01T00:00:00.000' AS DateTime), CAST(N'2029-12-31T00:00:00.000' AS DateTime), N'https://www.vegactu.com/actualite/style-de-vie/manger-vegan-a-barcelone-28405/', 1, 1)
GO
INSERT [dbo].[ACTUALITES] ([ID_ACTUALITE], [TITRE], [DESCRIPTION], [URL_IMAGE], [DATE_DEBUT], [DATE_FIN], [LINK], [ID_UTILISATEUR], [ACTIF]) VALUES (3, N'Mangez des 5 fruits et légumes par jour ', N'Il faut faire attention à votre alimentation....', N'https://www.vegactu.com/wp-content/uploads/2019/05/flaxkale.jpg', CAST(N'2019-01-01T00:00:00.000' AS DateTime), CAST(N'2029-12-31T00:00:00.000' AS DateTime), N'https://www.vegactu.com/actualite/style-de-vie/manger-vegan-a-barcelone-28405/', 1, 1)
GO
INSERT [dbo].[ACTUALITES] ([ID_ACTUALITE], [TITRE], [DESCRIPTION], [URL_IMAGE], [DATE_DEBUT], [DATE_FIN], [LINK], [ID_UTILISATEUR], [ACTIF]) VALUES (4, N'Faite du sport au moins trois fois par semaine ', N'Le sport est important pour entretenir la santé ....', N'https://www.vegactu.com/wp-content/uploads/2019/05/flaxkale.jpg', CAST(N'2019-01-01T00:00:00.000' AS DateTime), CAST(N'2029-12-31T00:00:00.000' AS DateTime), N'https://www.vegactu.com/actualite/style-de-vie/manger-vegan-a-barcelone-28405/', 1, 1)
GO
SET IDENTITY_INSERT [dbo].[ACTUALITES] OFF
GO
ALTER TABLE [dbo].[ACTUALITES] ADD  CONSTRAINT [DF_ACTUALITES_ACTIF]  DEFAULT ((1)) FOR [ACTIF]
GO
ALTER TABLE [dbo].[FACTURE] ADD  CONSTRAINT [DF_Table_1_FACTURE]  DEFAULT ((1)) FOR [FACTUREE]
GO
ALTER TABLE [dbo].[NOTE] ADD  CONSTRAINT [DF_NOTE_ACTIF]  DEFAULT ((1)) FOR [ACTIF]
GO
ALTER TABLE [dbo].[PRODUIT] ADD  CONSTRAINT [DF_PRODUIT_NOMBRE_DE_VENTE]  DEFAULT ((0)) FOR [NOMBRE_DE_VENTE]
GO
